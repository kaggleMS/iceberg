#coding=utf-8

from __future__ import print_function

try:
    from hyperopt import Trials, STATUS_OK, tpe
except:
    pass

try:
    from keras.datasets import mnist
except:
    pass

try:
    from keras.layers.core import Dense, Dropout, Activation
except:
    pass

try:
    from keras.models import Sequential
except:
    pass

try:
    from keras.utils import np_utils
except:
    pass

try:
    from hyperas import optim
except:
    pass

try:
    from hyperas.distributions import choice, uniform, conditional
except:
    pass

try:
    import numpy as np
except:
    pass

try:
    import tensorflow as tf
except:
    pass

try:
    from skimage.filters import frangi, hessian
except:
    pass

try:
    from hyperas.distributions import uniform
except:
    pass

try:
    from skimage.restoration import denoise_tv_chambolle, denoise_bilateral, denoise_wavelet, estimate_sigma, denoise_tv_bregman, denoise_nl_means
except:
    pass

try:
    from skimage.filters import gaussian
except:
    pass

try:
    from skimage.color import rgb2gray
except:
    pass

try:
    from skimage.filters import roberts, sobel, scharr, prewitt
except:
    pass

try:
    import keras
except:
    pass

try:
    import pandas as pd
except:
    pass

try:
    from sklearn.preprocessing import MinMaxScaler
except:
    pass

try:
    from keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Input, Flatten, GlobalAveragePooling2D, Lambda
except:
    pass

try:
    from keras.layers import GlobalMaxPooling2D
except:
    pass

try:
    from keras.layers.normalization import BatchNormalization
except:
    pass

try:
    from keras.layers.merge import Concatenate
except:
    pass

try:
    from keras.models import Model
except:
    pass

try:
    from keras.optimizers import Adam
except:
    pass

try:
    from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
except:
    pass

try:
    from sklearn.model_selection import train_test_split
except:
    pass

try:
    from sklearn.utils import shuffle
except:
    pass

try:
    from keras.preprocessing.image import ImageDataGenerator
except:
    pass

try:
    import time
except:
    pass

try:
    import os
except:
    pass
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperas.distributions import conditional

band_1, band_2, images = frame['band_1'].values, frame['band_2'].values, color_composite(frame)
to_arr = lambda x: np.asarray([np.asarray(item) for item in x])
band_1 = to_arr(band_1)
band_2 = to_arr(band_2)
band_3 = (band_1 + band_2) / 2
gray_reshape = lambda x: np.asarray([item.reshape(75, 75) for item in x])
# Make a picture format from flat vector
band_1 = gray_reshape(band_1)
band_2 = gray_reshape(band_2)
band_3 = gray_reshape(band_3)
print('Denoising and reshaping')
if train_b and clean_b:
    # Smooth and denoise data
    band_1 = smooth(denoise(band_1, weight_gray, False), smooth_gray)
    print('Gray 1 done')
    band_2 = smooth(denoise(band_2, weight_gray, False), smooth_gray)
    print('Gray 2 done')
    band_3 = smooth(denoise(band_3, weight_gray, False), smooth_gray)
    print('Gray 3 done')
    if do_frangi:
        band_1 = frangi_i(band_1)
        band_2 = frangi_i(band_2)
        band_3 = frangi_i(band_3)
    if do_sobel:
        band_1 = sobel_i(band_1)
        band_2 = sobel_i(band_2)
        band_3 = sobel_i(band_3)
    
if train_img and clean_img:
    images = smooth(denoise(images, weight_rgb, True), smooth_rgb)
    if do_frangi:
        images = frangi_i(images)
    if do_sobel:
        images = sobel_i(images)

print('RGB done')
tf_reshape = lambda x: np.asarray([item.reshape(75, 75, 1) for item in x])
band_1 = tf_reshape(band_1)
band_2 = tf_reshape(band_2)
band_3 = tf_reshape(band_3)
#images = tf_reshape(images)
band = np.concatenate([band_1, band_2, band_3], axis=3)
if labeled:
    y = np.array(frame["is_iceberg"])
else:
    y = None


def keras_fmin_fnct(space):

    '''zmienic zeby sie dalo load weights '''
    y_train, X_b, X_images = dataset
    y_train_full, y_val,\
    X_b_full, X_b_val,\
    X_images_full, X_images_val = train_test_split(y_train, X_b, X_images,
                                                   random_state=687, train_size=0.9)

    y_train, y_test, \
    X_b_train, X_b_test, \
    X_images_train, X_images_test = train_test_split(y_train_full, X_b_full,
                                                     X_images_full,
                                                     random_state=576, train_size=0.85)

    if train_b:
        if verbose > 0:
            print('Training bandwidth network')
        data_b1 = (X_b_train, y_train, X_b_test, y_test, X_b_val, y_val)
        model_b, model_b_cut = gen_model_weights(lr, 1e-6, 3, 'relu', batch_size,
                                                 max_epoch, log_dir+'/model_b',
                                                 data=data_b1, verbose=verbose,
                                                 log_dir=log_dir+'_bandwidth')

    if train_img:
        if verbose > 0:
            print('Training image network')
        data_images = (X_images_train, y_train, X_images_test, y_test, X_images_val, y_val)
        model_images, model_images_cut = gen_model_weights(lr, 1e-6, 3, 'relu',
                                                           batch_size, max_epoch,
                                                           log_dir+'/model_img',
                                                       data_images, verbose=verbose,log_dir=log_dir+'_images')

    if train_total:
        common_model = combined_model(model_b_cut, model_images_cut, lr/2, 1e-7)
        common_x_train = [X_b_full, X_images_full]
        common_y_train = y_train_full
        common_x_val = [X_b_val, X_images_val]
        common_y_val = y_val
        if verbose > 0:
            print('Training common network')
        callbacks = [ModelCheckpoint(log_dir+'/common', save_best_only=True, monitor='val_loss'),tb]
        if not from_disk:
         try:
            common_model.fit_generator(gen_flow_multi_inputs(X_b_full, X_images_full,
                                                             y_train_full, batch_size),
                                         epochs=common_epochs,
                                  steps_per_epoch=len(X_b_full) / batch_size,
                                  validation_data=(common_x_val, common_y_val), verbose=1,
                                  callbacks=callbacks)
         except KeyboardInterrupt:
            pass
        common_model.load_weights(filepath=log_dir+'/common')
        loss_val, acc_val = common_model.evaluate(common_x_val, common_y_val,
                                           verbose=0, batch_size=batch_size)
        loss_train, acc_train = common_model.evaluate(common_x_train, common_y_train,
                                                  verbose=0, batch_size=batch_size)
        if verbose > 0:
            print('Loss:', loss_val, 'Acc:', acc_val)
    if return_model:
        return common_model
    else:
        return (loss_train, acc_train), (loss_val, acc_val)

def get_space():
    return {
    }
